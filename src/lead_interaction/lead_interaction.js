import React, { Component } from 'react';
import { red } from 'ansi-colors';

let mystyle = {
    height: '600px',
    overflow: "auto"
}

let person = {
    border: '1px solid #ccc',
    padding: '5px',
    fontSize: '1em',
    cursor: 'move',
    boxShadow: '2px 2px 5px #ccc'
}

class LeadInteraction extends Component {

    constructor() {
        super();
        let date = new Date()

        let month = (date.getMonth() + 1) + "";
        let day = date.getDate() + "";
        let hours = date.getHours() + "";
        let minutes = date.getMinutes() + "";
        let years = date.getFullYear() + "";

        if (month.length < 2) {
            month = "0" + month;
        }

        if (day.length < 2) {
            day = "0" + day;
        }

        if (hours.length < 2) {
            hours = "0" + hours;
        }

        if (minutes.length < 2) {
            minutes = "0" + minutes;
        }

        this.state = {
            "current_date": `${years}-${month}-${day}T${hours}:${minutes}`
        }
    }

    render() {

        return (



            <div className="container-fluid register">
                <div className="row">
                    <div className="col-md-3 s register-left">
                        <img src="https://scontent.fgyd4-2.fna.fbcdn.net/v/t1.0-9/32086007_229958380890533_4187589368388517888_n.jpg?_nc_cat=101&_nc_ht=scontent.fgyd4-2.fna&oh=5e5aa5d944ba1ab6da06dc5612bd816d&oe=5D33F22B" />
                        <h1></h1>
                    </div>

                    <div className="col-md-9 register-right">
                        <div className="container">
                            <div className="row ">
                                <div className="col-lg-2 form-control " id="student" style={mystyle}>
                                    <h2>Leads</h2>

                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: 01.02.2019</div>
                                    </div>
                                    <br />
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: 01.02.2019</div>
                                    </div>
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: 01.02.2019</div>
                                    </div>
                                    <br />
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: 01.02.2019</div>
                                    </div>
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: 01.02.2019</div>
                                    </div>
                                    <br />
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: 01.02.2019</div>
                                    </div>
                                </div>
                                <br />
                                <div className="col-lg-2 form-control" id="call">
                                    <h2>Call</h2>
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: <input type="text" placeholder="Call tarixini yazin..." style={{ width: '100%' }} /></div>
                                        <div><button className="btn btn-primary">update</button></div>
                                    </div>
                                </div>
                                <div className="col-lg-2 form-control" id="recall">
                                    <h2>Recall</h2>
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: <input type="text" placeholder="Call tarixini yazin..." style={{ width: '100%' }} /></div>
                                        <div><button className="btn btn-primary">update</button></div>
                                    </div>
                                    <br />
                                    <div className="person" style={person}>
                                        <div>Elshad Agayev</div>
                                        <div>Phone: +994559617925</div>
                                        <div>Tarix: <input type="text" placeholder="Call tarixini yazin..." style={{ width: '100%' }} /></div>
                                        <div><button className="btn btn-primary">update</button></div>
                                    </div>
                                </div>
                                <div className="col-lg-2 form-control" id="meeting">
                                    <h2>Meeting</h2>
                                </div>
                                <div className="col-lg-2 form-control" id="status">
                                    <h2>Status</h2>
                                </div>
                            </div>


                        </div>

                    </div >

                </div>
            </div>


        )
    }
}


export default LeadInteraction





// <div>
//     <div className="jumbotron">
//         <h1>Lead interaction</h1>

//     </div>


{/* <div className="container" >

                    <form role="form">
                        <div className="form-group">
                            <label for="lead_id">Lead ID</label>
                            <input type="text" className="form-control" id="lead_id" placeholder="Lead ID" />
                        </div>
                        <div className="form-group">
                            <label for="call_date">Call Date</label>
                            <input type="datetime-local" className="form-control" id="call_date" placeholder="Call Date" value={this.state.current_date} />
                        </div>
                        <div className="form-group">
                            <label for="call_status">Called status</label>
                            <input type="text" className="form-control" id="call_status" placeholder="Called status" />
                        </div>
                        <div className="form-group">
                            <label for="called_at">Called At</label>
                            <input type="datetime-local" className="form-control" id="called_at" placeholder="Called at" />
                        </div>
                        <div className="form-group">
                            <label for="meeting_date">Meeting date</label>
                            <input type="datetime-local " className="form-control" id="meeting_date" placeholder="meeting date" />
                        </div>
                        <div className="form-group">
                            <label for="meeting_status">Meeting status</label>
                            <input type="text" className="form-control" id="meeting_status" placeholder="meeting status" />
                        </div>
                        <div className="form-group">
                            <label for="meet_at">Meet at</label>
                            <input type="datetime-local" className="form-control" id="meet_at" placeholder="meet at" />
                        </div>
                        <button type="submit" className="btn btn-primary">Save</button>
                    </form>

                </div>
                <br />
                <br />
                <br /> */}


{/* <div className="container">
                    <div className="row ">
                        <div className="col-lg-2 form-control" id="student" style={mystyle}>Student
                        </div>
                        <div className="col-lg-2" id="call">Call</div>
                        <div className="col-lg-2" id="recall">Recall</div>
                        <div className="col-lg-2" id="meeting">Meeting</div>
                        <div className="col-lg-2" id="status">Status</div>
                    </div>

                    <div className="row ">
                        <div >Tural Muradov</div>
                    </div>

                </div>

            </div > */}