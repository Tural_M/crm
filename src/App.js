import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './logo_tech.png'


class RegistryForm extends Component {
  render() {
    return (
<div className="container-fluid register">
                <div className="row">
                    <div className="col-md-3 s register-left">
                        <img src="https://scontent.fgyd4-2.fna.fbcdn.net/v/t1.0-9/32086007_229958380890533_4187589368388517888_n.jpg?_nc_cat=101&_nc_ht=scontent.fgyd4-2.fna&oh=5e5aa5d944ba1ab6da06dc5612bd816d&oe=5D33F22B"/>
                        <h1></h1>
                    </div>

                    <div className="col-md-9 register-right"> 

                        <div className="tab-content" id="myTabContent">

                            <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                                <h3 className="register-heading">Customer Relationship Management</h3>

                                <div className="row register-form">

                                    <div className="col-md-6">

                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Name *" value="" />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Surname *" />
                                        </div>
                                        <div className="form-group">
                                        <select className="form-control">
                                                <option className="hidden" selected disabled>Gender</option>
                                                <option>Male</option>
                                                <option>Female</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <input type="text" type="text" minlength="10" maxlength="10"  className="form-control"  placeholder="Phone"  />
                                        </div>

                                        <div className="form-group">
                                            <input type="email" className="form-control" placeholder="Email address" />
                                        </div>
                                            <div className="form-group">
                                            
                                        </div>

                                    </div>


                                    <div className="col-md-6">
                                        
                                        <div className="form-group">
                                            <input type="text"  className="form-control" placeholder="Instagram"  />
                                        </div>

                                        <div className="form-group">
                                            <input type="text"  className="form-control" placeholder="Facebook"  />
                                        </div>
                                        
                                        <div className="">
                                          <div class="form-group">
                                              <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                                  <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4" placeholder="Visited at:"/>
                                                  <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                  </div>
                                              </div>
                                          </div>
                                        </div>


                                        <div className="">
                                          <div class="form-group">
                                                <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
                                                    <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <select className="form-control">
                                                <option className="hidden" selected disabled>Status</option>
                                                <option>status1</option>
                                                <option>status2</option>
                                            </select>
                                        <input type="submit" className="btnRegister"  value="Register"/>

                                        <div className="form-group">
                                        
                                        </div>

                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>

            </div>
    );
  }
}

export default RegistryForm