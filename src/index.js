import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import RegistryForm from './lead_registration/lead_registration';
import LeadInteraction from "./lead_interaction/lead_interaction"
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<LeadInteraction />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
